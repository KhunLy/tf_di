﻿namespace DI
{
    interface IImprimante
    {
        void Imprimer(string document);
    }
}
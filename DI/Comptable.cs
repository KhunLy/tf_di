﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DI
{
    class Comptable
    {
        public IImprimante Imprimante;

        public Comptable(IImprimante imprimante)
        {
            this.Imprimante = imprimante;
        }

        public void ImprimerFichesalaire()
        {
            this.Imprimante.Imprimer("Fiche Salaire");
        }
    }
}

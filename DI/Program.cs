﻿using System;

namespace DI
{
    class Program
    {
        static void Main(string[] args)
        {
            IImprimante imprimante = new ImprimanteBrother();
            Directeur Mike = new Directeur(imprimante);
            Secretaire Monique = new Secretaire(imprimante);
            Comptable Jean = new Comptable(imprimante);
            while (true)
            {
                Console.WriteLine("1. Demande Mike");
                Console.WriteLine("2. Demande Monique");
                Console.WriteLine("3. Demande Jean");
                int choix = int.Parse(Console.ReadLine());
                switch (choix)
                {
                    case 1:
                        Mike.ImprimerRapports();
                        break;
                    case 2:
                        Monique.ImprimerBonDeCommande();
                        break;
                    case 3:
                        Jean.ImprimerFichesalaire();
                        break;
                }
            }
        }
    }
}

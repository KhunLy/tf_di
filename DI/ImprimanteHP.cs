﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DI
{
    class ImprimanteHP : IImprimante
    {
        public void Imprimer(string document)
        {
            Console.WriteLine($"{document} est imprimé avec l'imprimante HP");
        }
    }
}

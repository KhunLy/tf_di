﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DI
{
    class Secretaire
    {
        public IImprimante Imprimante;

        public Secretaire(IImprimante imprimante)
        {
            this.Imprimante = imprimante;
        }

        public void ImprimerBonDeCommande()
        {
            this.Imprimante.Imprimer("Bon de commande");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DI
{
    class ImprimanteBrother : IImprimante
    {
        public void Imprimer(string document)
        {
            Console.WriteLine($"{document} est imprimé avec l'imprimante Brother");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DI
{
    class Directeur
    {
        public IImprimante Imprimante;

        public Directeur(IImprimante imprimante)
        {
            this.Imprimante = imprimante;
        }
        public void ImprimerRapports()
        {
            this.Imprimante.Imprimer("Rapports");
        }
    }
}
